# golang-Post

| Post | GET |
| ------ | ------ |
| Unmarshal | Marshal |
| Decode | Encode | 
| [File](https://gitlab.com/zendrulat123/goflutters/-/blob/master/API/API.go) | [File](https://gitlab.com/zendrulat123/goflutters/-/blob/master/API/Send.go) | 



**Remember to update the Headers, Cors, and use curls first.**

Also Remember in the terminal to run `netstat -a` to get all the ports and to see your programs assigned ip/port numbers that you need 
to update and allow your headers to connect to.


This is the break down.


1.Remember to divide the request into Get and Post.


`switch r.Method { case "GET":  case "POST":`


2. Get the body request


`r.Body = http.MaxBytesReader(w, r.Body, 1048576)
reqBody, err := ioutil.ReadAll(r.Body)
`


3. Unmarshal the data because it is json.


`l, err := UnmarshalLogin(reqBody)`


4. Open the database.


`db, err := sql.Open("mysql", "root:@/users")`


5. Query the database.


`stmt, err := db.Prepare("INSERT INTO users(email, username, password) VALUES(?, ?, ?)")
		if err != nil {
			log.Fatal(err)
		}
		userstemp := Login{Email: l.Email, Username: l.Username, Password: l.Password}
		fmt.Println(userstemp)
		res, err := stmt.Exec(userstemp.Email, userstemp.Username, userstemp.Password)

`


6. Write header back


`w.WriteHeader(http.StatusOK)`





***For Sending data do the following***




1. Remember to check the request


`switch r.Method { case "GET": `

2. Open the database


`db, err := sql.Open("mysql", "root:@/users")`

3. Qeury the database


`rows, err := db.Query("select * from users where id = ?", 1)
for rows.Next() {

			err := rows.Scan(&id, &email, &username, &password)
			if err != nil {
				fmt.Println(err)
			} else {

				i++
				fmt.Println("scan ", i)
			}

			login := append(login, Login{Email: email, Username: username, Password: password})
			fmt.Println("before marshal ", login)
			
			json.NewEncoder(w).Encode(login) //remember to encode it

		}`


4. change the header for the json


`w.Header().Set("Content-Type", "application/json")`